import { SET_BUGS, REVIEW_BUG } from '../constants';

export default (state = [], action) => {
    switch(action.type) {
        case SET_BUGS:
            const { bugs } = action;
            
            return bugs;

        case REVIEW_BUG:
            return state.map(b =>
                {
                    if (b._id !== action._id) {
                        return b;
                    }
    
                    return {
                        ...b,
                        reviewed: !b.reviewed
                    }
                }
            );
            
        default:
            return state;
    }
}