import { SET_USERS, TOGGLE_USER } from '../constants';

export default (state = [], action) => {
    switch(action.type) {
        case SET_USERS:
            const { users } = action;
            
            return users;
            
        case TOGGLE_USER:
            return state.map(u =>
                {
                    if (u.uid !== action.uid) {
                        return u;
                    }

                    return {
                        ...u,
                        type: u.type === "Administrator" ? "Tester" : "Administrator"
                    }
                }
            );
            
        default:
            return state;
    }
}