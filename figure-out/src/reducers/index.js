import { combineReducers } from 'redux';
import user from './reducer_user';
import entries from './reducer_diary';
import users from './reducer_users';
import bugs from './reducer_bug';

export default combineReducers({
    user,
    entries,
    users,
    bugs
})