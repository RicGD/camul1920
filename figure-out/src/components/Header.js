import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firebaseApp } from '../firebase';
import profile from '../assets/profile.png';
import { Navbar, Nav } from 'react-bootstrap';

class Header extends Component {

    signOut() {
        firebaseApp.auth().signOut()
            .catch(error => {
                this.setState({error});
            })
    }

    render() {
        return (
            <div>
                <Navbar className="nav-bar" expand="lg">
                    <Navbar.Brand href="/" className="nav-brand">Figure Out</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ml-auto">
                            <Nav.Link href="/">Inicio</Nav.Link>
                            {/*<Nav.Link href="">Sobre</Nav.Link>
                            <Nav.Link href="">Funcionalidades</Nav.Link>*/}
                            {
                                this.props.user.email === null ? (
                                    <Nav.Link className="nav-link-login" href="/signin">Login/Registar</Nav.Link>
                                ) : (
                                    <Nav.Link href="/profile">
                                        <img src={profile} alt='profile' className="header-img" />
                                        {this.props.user.displayName === null ? this.props.user.email.split("@")[0] : this.props.user.displayName}
                                    </Nav.Link>
                                )
                            }
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                {this.props.children}
                <div className="footer-section">
                    <p className="footer">Todos os direitos Reservados © 2020</p>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state;
    return {
        user
    }
}

export default connect(mapStateToProps, null)(Header);