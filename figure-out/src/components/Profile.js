import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProfileTitle from './ProfileTitle';
import { loadUser } from '../actions';
import { API_ADDRESS } from '../constants';

class Profile extends Component {

    componentDidMount() {
        fetch(`${API_ADDRESS}/users/${this.props.user.uid}`)
            .then(response => response.json())
            .then(json => {
                if (json.length > 0) {
                    const user = json[0];
                    this.props.loadUser(user);
                }
            })
            .catch(error => alert(error.message));
    }

    render() {
        return (
            <div style={{paddingRight: '10%'}}>
                <ProfileTitle text="A tua conta" />
                <h2 className="profile-subtitle-section">Perfil</h2>
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        Email
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        {this.props.user.email}
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        Data de Nascimento
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        {this.props.user.birthDate}
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        País
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        {this.props.user.country}
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb'}} />
                <div>
                    <div className="profile-data-name" style={{width: '50%', display: 'inline-block'}}>
                        Tipo de Utilizador
                    </div>
                    <div className="profile-data" style={{width: '50%', display: 'inline-block'}}>
                        {this.props.user.type}
                    </div>
                </div>
                <hr style={{border: 'solid 0.5px #ebebeb'}} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state;
    return {
        user
    }
}

export default connect(mapStateToProps, { loadUser })(Profile);