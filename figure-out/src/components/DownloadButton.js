import React, { Component } from 'react';
import android from '../assets/android.png';
import apple from '../assets/apple.png';
import manual from '../assets/manual.png';
import { ANDROID_APP_URL, IOS_APP_URL } from '../constants';

class DownloadButton extends Component {

    downloadApp(os) {
        if (os === 'Android') {
            var url = process.env.PUBLIC_URL + '/figure-out-app-for-android.apk';
            var a = document.createElement('a');
            a.href = url;
            a.download = "figure-out-app-for-android.apk";
            document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
            a.click();
            a.remove(); //afterwards we remove the element again

            /*
            fetch(`${ANDROID_APP_URL}`)
                .then(response => response.blob())
                .then(blob => {
                    var url = window.URL.createObjectURL(blob);
                    var a = document.createElement('a');
                    a.href = url;
                    a.download = "figure-out-app-for-android.apk";
                    document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
                    a.click();
                    a.remove(); //afterwards we remove the element again
                })
                .catch(error => alert(error.message));*/
        } else {
            //Basta definir o url no constants.js e descomentar o código
            
            /*fetch(`${IOS_APP_URL}`)
                .then(response => response.blob())
                .then(blob => {
                    var url = window.URL.createObjectURL(blob);
                    var a = document.createElement('a');
                    a.href = url;
                    a.download = "figure-out-app-for-iOS.apk";
                    document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
                    a.click();
                    a.remove(); //afterwards we remove the element again
                })
                .catch(error => alert(error.message));*/
            return;
        }
    }

    onClickHandler() {
        var url = process.env.PUBLIC_URL + '/FigureOutUserManual.pdf';
        var a = document.createElement('a');
        a.href = url;
        a.download = "FigureOutUserManual.pdf";
        document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
        a.click();
        a.remove(); //afterwards we remove the element again
    }

    render() {
        return (
            <div className="section4">
                <div>
                    <p className="section4-text">Download</p>
                    <button className="download-button" onClick={() => this.downloadApp('Android')}><img className="button-icon" src={android} alt='android' /> Android</button>
                    <button className="download-button" onClick={() => this.downloadApp('iOS')}><img className="button-icon" src={apple} alt='apple' /> iOS</button>
                </div>
                <div>
                    <button className="download-button" onClick={() => this.onClickHandler()}><img className="button-icon" src={manual} alt='manual' /> Manual</button>
                </div>
            </div>
        )
    }
}

export default DownloadButton;