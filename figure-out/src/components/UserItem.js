import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toggleUser } from '../actions';
import { API_ADDRESS } from '../constants';

class UserItem extends Component {

    handleOnClick() {
        this.props.toggleUser(this.props.userSubject.uid);

        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: this.props.userSubject.email,
                phoneNumber: this.props.userSubject.phoneNumber,
                displayName: this.props.userSubject.displayName,
                photoURL: this.props.userSubject.photoURL,
                birthDate: this.props.userSubject.birthDate,
                country: this.props.userSubject.country,
                type: this.props.userSubject.type === "Administrator" ? "Tester" : "Administrator"
            })
        };

        fetch(`${API_ADDRESS}/users/${this.props.userSubject.uid}`, requestOptions)
            .then(response => response.json())
            .catch(error => alert(error.message));
    }

    render() {
        return (
            <div>
                <div className="profile-data-name" style={{ width: '50%', display: 'inline-block' }}>{this.props.userSubject.email}</div>
                <div className="profile-data-name" style={{ width: '50%', display: 'inline-block', textAlign: 'right', color: 'black' }}>
                    <input type="radio" checked={this.props.userSubject.type === "Administrator" ? 'checked' : ''} onClick={() => this.handleOnClick()} />&nbsp;Admin
                </div>
                <hr style={{ border: 'solid 0.5px #ebebeb' }} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state;
    return {
        user
    }
}

export default connect(mapStateToProps, { toggleUser })(UserItem);