import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../index.css';
import { firebaseApp } from '../firebase';
import profile from '../assets/profile.png';
import accountImg from '../assets/account-img.png';
import editImg from '../assets/edit-img.png';
import editPasswordImg from '../assets/edit-password-img.png';
import gestaoContasImg from '../assets/gestao-contas-img.png';
import historyImg from '../assets/history-img.png';
import statisticsImg from '../assets/statistics-img.png';
import feedbackImg from '../assets/feedback-img.png';
import logoutImg from '../assets/logout-img.png';
import bugsImg from '../assets/bugs-img.png';

class SideMenu extends Component {

    signOut() {
        firebaseApp.auth().signOut()
            .catch(error => {
                this.setState({error});
            })
    }

    render() {
        var Gerir;
        var Bugs;
        if (this.props.user.type === 'Administrator') {
            Gerir = (
                <div>
                    <hr style={{ border: 'solid 0.5px #4c4b4b' }} />
                    <div>
                        <a href={'/account-promotion'}>
                            <img src={gestaoContasImg} alt='gestao-contas-img' className="side-bar-img" />
                            Gestão de Contas</a>
                    </div>
                </div>
            );
            Bugs = (
                <div>
                    <hr style={{ border: 'solid 0.5px #4c4b4b' }} />
                    <div>
                        <a href={'/bugs'}>
                            <img src={bugsImg} alt='bugs-img' className="side-bar-img" />
                            Bugs</a>
                    </div>
                </div>
            );
        }

        return (
            <div>
                <div style={{ width: '35%', display: 'inline-block' }}>
                    <div className="side-bar">
                        <br />
                        <div style={{textAlign: 'center'}}>
                            <img src={profile} alt='profile' className="profile-img" />
                        </div>
                        <br />
                        <div>
                            <a href={'/profile'}>
                                <img src={accountImg} alt='account-img' className="side-bar-img" />
                                Conta</a>
                        </div>
                        <hr style={{ border: 'solid 0.5px #4c4b4b' }} />
                        <div>
                            <a href={'/edit'}>
                                <img src={editImg} alt='edit-img' className="side-bar-img" />
                                Editar Perfil</a>
                        </div>
                        <hr style={{ border: 'solid 0.5px #4c4b4b' }} />
                        <div>
                            <a href={'/change-password'}>
                                <img src={editPasswordImg} alt='edit-password-img' className="side-bar-img" />
                                Alterar Password</a>
                        </div>
                        { Gerir }
                        {/*
                        <hr style={{ border: 'solid 0.5px #4c4b4b' }} />
                        <div>
                            <a href={'/history'}>
                                <img src={historyImg} alt='history-img' className="side-bar-img" />
                                Histórico</a>
                        </div>*/}
                        <hr style={{ border: 'solid 0.5px #4c4b4b' }} />
                        <div>
                            <a href={'/statistics'}>
                                <img src={statisticsImg} alt='statistics-img' className="side-bar-img" />
                                Estatísticas</a>
                        </div>
                        <hr style={{ border: 'solid 0.5px #4c4b4b' }} />
                        <div>
                            <a href={'/feedback'}>
                                <img src={feedbackImg} alt='feedback-img' className="side-bar-img" />
                                Diário</a>
                        </div>
                        { Bugs }
                        <hr style={{ border: 'solid 0.5px #4c4b4b' }} />
                        <div style={{ marginBottom: '10%' }}>
                            <a href={'/signIn'} onClick={() => this.signOut()}>
                                <img src={logoutImg} alt='logout-img' className="side-bar-img" />
                                Sair</a>
                        </div>
                        <br />
                    </div>
                </div>
                <div style={{ width: '65%', display: 'inline-block', verticalAlign: 'top' }}>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state;
    return {
        user
    }
}

export default connect(mapStateToProps, null)(SideMenu);