import React from 'react';

const ProfileTitle = props => {
    return (
        <div>
            <h1 className="profile-title-section">{props.text}</h1>
            <hr style={{border: '2px solid #16a085'}}/>
        </div>
    )
}

export default ProfileTitle;