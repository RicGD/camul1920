var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
const Entry = require('../models/entry');
var storage = require('../utils/storage')
var fs = require('fs');

var upload = storage.upload;

//Create a new mobile diary entry in the DB
router.post('/', upload.array('file', 3), (request, response, next) => {

    var names = []

    if (!request.files == undefined) {
        request.files.forEach(element => {
            names.push(element.filename)
            storage.uploadFile('figure-out', './files/' + element.filename)
                .then(function () {
                    fs.unlinkSync('./files/' + element.filename)
                }).catch(error => {
                    response.status(500).json({
                        error: "Error uploading media"
                    })
                    fs.unlinkSync('./files/' + element.filename)
                })
        });
    }

    const entry = new Entry({
        _id: new mongoose.Types.ObjectId(),
        title: request.body.title,
        description: request.body.description,
        files: names,
        user: request.body.user,
        reviewed: false,
        timestamp: request.body.timestamp
    })

    entry.save()
        .then(res => {
            response.status(201).json({
                id: res.id
            })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//Retrieve all entries
router.get('/', (request, response, next) => {
    Entry.find()
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//Retrieve a mobile diary entry DB
router.get('/:entryId', (request, response, next) => {
    const entryId = request.params.entryId;

    Entry.findById(entryId)
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//Retrieve all entries from a single user
router.get('/user/:uid', (request, response, next) => {
    const uid = request.params.uid;

    Entry.find({ user: uid })
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//Update an entry given its ID
router.put('/:entryId', upload.array('file', 3), (request, response, next) => {
    const entryId = request.params.entryId;

    const entry = {
        title: request.body.title,
        description: request.body.description,
        reviewed: false,
        timestamp: request.body.timestamp
    }

    Entry.findByIdAndUpdate(entryId, entry, { new: true })
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

router.put('/review/:entryId', (request, response, next) => {
    const entryId = request.params.entryId;

    Entry.findById(entryId)
        .exec()
        .then(doc => {
            const entry = {
                title: doc.title,
                description: doc.description,
                timestamp: doc.timestamp,
                reviewed: request.body.reviewed
            }

            Entry.findByIdAndUpdate(entryId, entry, { new: true })
                .exec()
                .then(doc => {
                    response.status(200).json(doc)
                })
                .catch(error => {
                    response.status(500).json({
                        error: error
                    })
                })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//Delete an entry given its ID
router.delete('/:entryId', (request, response, next) => {
    const entryId = request.params.entryId;

    Entry.findByIdAndDelete(entryId)
        .exec()
        .then(doc => {
            response.status(200).json({
                deleted: "yes"
            })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

module.exports = router;