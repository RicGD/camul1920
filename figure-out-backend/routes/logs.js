var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Log = require('../models/log');

//Create a new activity log
router.post('/', (request, response, next) => {

    const log = new Log({
        _id: new mongoose.Types.ObjectId(),
        latitude: request.body.latitude,
        longitude: request.body.longitude,
        timestamp: request.body.timestamp,
        user: request.body.user
    })

    log.save()
        .then(res => {
            response.status(201).json({
                id: res.id
            })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
});

//retrieves a specific log given its id
router.get('/:logId', (request, response, next) => {
    const logId = request.params.logId;

    Log.findById(logId)
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
});

//Retrieve all logs from a single user
router.get('/user/:uid', (request, response, next) => {
    const uid = request.params.uid;

    Log.find({ user: uid })
        .exec()
        .then(doc => {
            response.status(200).json(doc)
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

//Delete a certain log
router.delete('/:logId', (request, response, next) => {
    const logId = request.params.logId;

    Log.findByIdAndDelete(logId)
        .exec()
        .then(doc => {
            response.status(200).json({
                deleted: "yes"
            })
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

module.exports = router;