const { Expo } = require('expo-server-sdk')
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
const Token = require('../models/token');

// Create a new Expo SDK client
let expo = new Expo();

// Create the messages that you want to send to clients
router.post('/send', (request, response, next) => {
    let messages = []
    let somePushTokens = []

    Token.find()
        .exec()
        .then(doc => {
            doc.forEach(token => {
                somePushTokens.push(token.token)

                for (let pushToken of somePushTokens) {
                    // Each push token looks like ExponentPushToken[xxxxxxxxxxxxxxxxxxxxxx]

                    // Check that all your push tokens appear to be valid Expo push tokens
                    if (!Expo.isExpoPushToken(pushToken)) {
                        console.error(`Push token ${pushToken} is not a valid Expo push token`);
                        continue;
                    }

                    // Construct a message (see https://docs.expo.io/versions/latest/guides/push-notifications)
                    messages.push({
                        to: pushToken,
                        sound: 'default',
                        body: request.body.message,
                        data: { withSome: 'data' },
                        channelId: 'default'
                    })
                }

                // The Expo push notification service accepts batches of notifications so
                // that you don't need to send 1000 requests to send 1000 notifications. We
                // recommend you batch your notifications to reduce the number of requests
                // and to compress them (notifications with similar content will get
                // compressed).
                let chunks = expo.chunkPushNotifications(messages);
                let tickets = [];
                (async () => {
                    // Send the chunks to the Expo push notification service. There are
                    // different strategies you could use. A simple one is to send one chunk at a
                    // time, which nicely spreads the load out over time:
                    for (let chunk of chunks) {
                        try {
                            let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
                            console.log(ticketChunk);
                            tickets.push(...ticketChunk);
                            // NOTE: If a ticket contains an error code in ticket.details.error, you
                            // must handle it appropriately. The error codes are listed in the Expo
                            // documentation:
                            // https://docs.expo.io/versions/latest/guides/push-notifications#response-format
                        } catch (error) {
                            console.error(error);
                        }
                    }
                    response.status(201).json({
                        sent: "ok"
                    })
                })();
            });
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })


})

//saves a new token to the db
router.post('/', (request, response, next) => {
    const token = new Token({
        _id: new mongoose.Types.ObjectId(),
        token: request.body.token
    })

    Token.find({ token: request.body.token })
        .exec()
        .then(res => {
            if (res.length == 0) {
                token.save()
                    .then(res => {
                        response.status(201).json({
                            id: res.id
                        })
                    })
                    .catch(error => {
                        response.status(500).json({
                            error: error
                        })
                    })
            } else {
                response.send("This token already exists");
            }
        })
        .catch(error => {
            response.status(500).json({
                error: error
            })
        })
})

module.exports = router;