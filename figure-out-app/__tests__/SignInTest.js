import React, { Component } from 'react';
import { configure } from 'enzyme';
import { shallow } from 'enzyme';
import Login from '../src/screens/SignIn';
import { firebaseApp } from '../firebase';

import Adapter from 'enzyme-adapter-react-16';


configure({ adapter: new Adapter() });


const createTestProps = (props) => ({
  navigation: {
    navigate: jest.fn(),
    setOptions: jest.fn()
  },
  ...props
});


describe("Login", () => {
  describe("Rendering", () => {
    let wrapper;
    let props;
    beforeEach(() => {
      props = createTestProps({});
      wrapper = shallow(<Login {...props} />);
    });


    it("rendered correctly", () => {
      expect(wrapper.state('isLogged')).toEqual(undefined);
    });

    it("Login methods are called on login button press", () => {

      const signInSpy = jest.spyOn(wrapper.instance(), "signIn");
      const checkCredentialsSpy = jest.spyOn(wrapper.instance(), "checkCredentials");
      const showErrorMessageSpy = jest.spyOn(wrapper.instance(), "showErrorMessage");

      wrapper.setState({ email: "apptest@figureout.com", password: "figureout" })
      wrapper.find('TouchableOpacity[name="loginBtn"]').simulate('press');

      expect(checkCredentialsSpy).toHaveBeenNthCalledWith(1, "apptest@figureout.com", "figureout");
      expect(checkCredentialsSpy).toHaveReturned();
      expect(signInSpy).toHaveBeenCalledTimes(1);
      expect(showErrorMessageSpy).toHaveBeenCalledTimes(0);

    });

    it("Navigates to register screen on button press", async () => {

      wrapper.find('TouchableOpacity[name="RegisterBtn"]').simulate('press');
      await tick();
      expect(props.navigation.navigate).toHaveBeenNthCalledWith(1, 'SignUpScreen');


    });

  });

});


function tick() {
   return new Promise(resolve => {
     setTimeout(resolve, 0);
   })
 }

