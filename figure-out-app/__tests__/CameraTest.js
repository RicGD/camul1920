import React, { Component } from 'react';
import { configure } from 'enzyme';
import { shallow } from 'enzyme';
import CameraComponent from '../src/screens/Camera';
import { firebaseApp } from '../firebase';

import Adapter from 'enzyme-adapter-react-16';


configure({ adapter: new Adapter() });


const createTestProps = (props) => ({
  navigation: {
    navigate: jest.fn(),
    setOptions: jest.fn()
  },
  useIsFocused: jest.fn(),
  ...props
});

/**
describe("Camera", () => {
  describe("Rendering", () => {
    let wrapper;
    let props;
    let useIsFocused;
    beforeEach(() => {
      props = createTestProps({});
      useIsFocused = jest.fn();
      wrapper = shallow(<CameraComponent {...props} useIsFocused={useIsFocused} />);
    });


    it("rendered correctly", () => {
      expect(wrapper.state('hasCameraPermission')).toEqual(false);
    });

    it("Camera methods are called on camera button press", () => {

      const takePictureSpy = jest.spyOn(wrapper.instance(), "takePicture");

      wrapper.find('TouchableOpacity[name="cameraBtn"]').simulate('press');

      expect(takePictureSpy).toHaveBeenCalledTimes(1);


    });

  });

});
*/

it("placeholder", () => {

  expect(true);

});



function tick() {
   return new Promise(resolve => {
     setTimeout(resolve, 0);
   })
 }

