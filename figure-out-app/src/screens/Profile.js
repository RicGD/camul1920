import React, { Component, useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Picker,
  Alert,
  TouchableHighlight
} from 'react-native';
import i18n from 'i18n-js';
import { firebaseApp } from '../../firebase';
import { EntriesToSendList, AllEntriesList } from '../model/DiaryEntryList'
import {BACKEND_URL} from '../../config'


export default class Secured extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {
        birthDate:"",
        country:"",
      }
    }
    this.getUserAsync();
  }

  getUserAsync(){
    var self = this
    fetch(BACKEND_URL + '/users/' + firebaseApp.auth().currentUser.uid, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(response => {
      if (response.status === 200) {
        JSON.stringify(response.json().then(function (data) {
          console.log(data[0])
          self.setState({ user: data[0] })
        }))
      } else {
        JSON.stringify(response.json().then(function (data) {
          console.log(data)
        }))
      }
    });
  }
  render() {
    //console.log(this.props)
    this.props.navigation.setOptions({
      headerShown: false,

    })

    const {navigate} = this.props.navigation
    return (
      <View style={styles.container}>
        <Text style={styles.welcomeText}>{i18n.t('SCREEN_PROFILE')}</Text>
        <View style={styles.line} />
        <View style={styles.infopanel}>
          <Text style={styles.LabelText}>{i18n.t('PROFILE_EMAIL_LABEL')}</Text>
          <Text style={styles.LabelTextInline}>{firebaseApp.auth().currentUser.email}</Text>
        </View>
        <View style={styles.line2} />
        <View style={styles.infopanel}>
          <Text style={styles.LabelText}>{i18n.t('PROFILE_BIRTH_DATE_LABEL')}</Text>
          <Text style={styles.LabelTextInline}>{this.state.user.birthDate}</Text>
        </View>
        <View style={styles.line2} />
        <View style={styles.infopanel}>
          <Text style={styles.LabelText}>{i18n.t('PROFILE_COUNTRY_LABEL')}</Text>
          <Text style={styles.LabelTextInline}>{this.state.user.country}</Text>
        </View>
        <View style={styles.line2} />
        <View style={{width:"85%", justifyContent: 'space-between', height: "30%", alignSelf: 'center', marginTop: 40}}>
        <TouchableOpacity onPress={() => navigate('ReportBug')} style={styles.BugBtn}>
          <Text style={styles.BtnText}>{i18n.t('PROFILE_REPORT')}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigate('About')} style={styles.BugBtn}>
          <Text style={styles.BtnText}>{i18n.t('PROFILE_ABOUT')}</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.signOut.bind(this)} style={styles.LogoutBtn}>
          <Text style={styles.BtnText}>{i18n.t('PROFILE_LOGOUT_BTN')}</Text>
        </TouchableOpacity>
        </View>
      </View>
    )
  }

  signOut() {
    AllEntriesList.length = 0;
    EntriesToSendList.length = 0;
    firebaseApp.auth().signOut()
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#31b39a',
  },
  infopanel: {
    marginTop: 10,
    marginLeft: 40,
  },
  line: {
    width: "85%",
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 40,
    borderBottomColor: 'white',
    borderBottomWidth: 2,
  },
  line2: {
    width: "100%",
    alignSelf: 'center',
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    marginTop: "-5%"
  },
  welcomeText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 40,
    color: "#fff",
    marginTop: 60,
    marginLeft: 40,
    marginBottom: -30,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },

  LabelText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    height: 30
  },
  LabelTextInline: {
    color: "#fff",
    marginLeft: 40,
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    height: 30,
    marginBottom: 20,
  },
  LogoutBtn: {
    color: "#000",
    fontFamily: 'Poppins-Regular',
    fontSize: 24,
    width: "85%",
    backgroundColor: "#98d9cc",
    borderRadius: 20,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
  },
  BugBtn: {
    color: "#000",
    fontFamily: 'Poppins-Regular',
    fontSize: 24,
    width: "85%",
    backgroundColor: "#98d9cc",
    borderRadius: 20,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
  },
  BtnText: {
    marginTop: 10,
    color: "#000",
    fontFamily: 'Poppins-Regular',
    fontSize: 24,
    height: 52,


  },
});