import React, { Component, useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Picker,
  Alert,
  TouchableHighlight
} from 'react-native';
import i18n from 'i18n-js';
import { firebaseApp } from '../../firebase';
import { EntriesToSendList, AllEntriesList } from '../model/DiaryEntryList'
import {BACKEND_URL} from '../../config'


export default class Secured extends Component {

  constructor(props) {
    super(props);
    this.state = {
      
    }
  }

  render() {
    //console.log(this.props)
    this.props.navigation.setOptions({
      title: i18n.t('SCREEN_ABOUT'),
      //Sets Header text of Status Bar
      headerTitleStyle: {
        fontFamily: 'Poppins-Regular',
        fontSize: 30,
      },
    });

    const {navigate} = this.props.navigation
    return (
      <View style={styles.container}>
        <Text style={styles.welcomeText}>{i18n.t('SCREEN_ABOUT_TITLE')}</Text>
        <View style={styles.line} />
        <View style={styles.infopanel}>
          <Text style={styles.LabelText}>{i18n.t('SCREEN_ABOUT_TEXT_VERSION')}</Text>
          <Text style={styles.LabelText}>{i18n.t('SCREEN_ABOUT_TEXT_DESCRIPTION')}</Text>
          <Text style={styles.LabelText}>{i18n.t('SCREEN_ABOUT_TEXT_AUTHOR')}</Text>
        </View>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#31b39a',
  },
  infopanel: {
    width: "85%",
    alignSelf: "center"
  },
  line: {
    width: "85%",
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 40,
    borderBottomColor: 'white',
    borderBottomWidth: 2,
  },
  line2: {
    width: "100%",
    alignSelf: 'center',
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    marginTop: "-5%"
  },
  welcomeText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 40,
    color: "#fff",
    width: "85%",
    alignSelf: 'center',
    marginBottom: -30,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },

  LabelText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    height: 30
  },
  LabelTextInline: {
    color: "#fff",
    marginLeft: 40,
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    height: 30,
    marginBottom: 20,
  },
  LogoutBtn: {
    color: "#000",
    fontFamily: 'Poppins-Regular',
    fontSize: 24,
    width: "85%",
    backgroundColor: "#98d9cc",
    borderRadius: 20,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  BugBtn: {
    marginTop:100,
    color: "#000",
    fontFamily: 'Poppins-Regular',
    fontSize: 24,
    width: "85%",
    backgroundColor: "#98d9cc",
    borderRadius: 20,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    marginBottom: 10
  },
  BtnText: {
    marginTop: 10,
    color: "#000",
    fontFamily: 'Poppins-Regular',
    fontSize: 24,
    height: 52,


  },
});