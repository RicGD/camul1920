import React, { Component, useState, useEffect } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Alert,
    Image,
    SafeAreaView,
} from 'react-native';
import i18n from 'i18n-js';
import * as Permissions from 'expo-permissions';
import * as MediaLibrary from 'expo-media-library';
import { Audio } from 'expo-av'

var recording = new Audio.Recording();
export default class RecordAudioScreen extends Component {

    constructor(props) {
        super(props);
        this.recording = null;
        this.state = {
            isRecording: false,
            timerOn: false,
            timerStart: 0,
            timerTime: 0,
        }
    }

    async componentDidMount() {
        await this.checkRecordAudioPermission();
    }

    async checkRecordAudioPermission() {
        await Permissions.askAsync(Permissions.AUDIO_RECORDING)
        const status = await Permissions.getAsync(Permissions.AUDIO_RECORDING);
        if (status.status === 'granted') {
            this.setState({ hasAudioPermission: true })
        }

    }

    startTimer() {
        this.setState({
            timerOn: true,
            timerTime: 0,
            timerStart: Date.now(),

        });
        this.timer = setInterval(() => {
            this.setState({
                timerTime: Date.now() - this.state.timerStart
            });
        }, 10);
    }

    handleOnPressIn = () => {
        this.startRecording();
    }

    handleOnPressOut = () => {
        this.stopRecording();
        this.getTranscription();
    }

    render() {

        this.props.navigation.setOptions({
            title: i18n.t('RECORD_SCREEN'),
            //Sets Header text of Status Bar
            headerTitleStyle: {
              fontFamily: 'Poppins-Regular',
              fontSize: 30,
            },
          });
        const { isRecording } = this.state;
        const { timerTime } = this.state;
        let centiseconds = ("0" + (Math.floor(timerTime / 10) % 100)).slice(-2);
        let seconds = ("0" + (Math.floor(timerTime / 1000) % 60)).slice(-2);
        let minutes = ("0" + (Math.floor(timerTime / 60000) % 60)).slice(-2);

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.container}>
                    <Text style={styles.welcomeText}>{minutes}:{seconds}:{centiseconds}</Text>
                    <TouchableOpacity
                        style={styles.RecordButton}
                        onPressIn={this.handleOnPressIn}
                        onPressOut={this.handleOnPressOut}
                    >
                        <Image style={styles.icon} source={require('../../assets/ui/microphone_black_icon.png')} />
                    </TouchableOpacity>
                    <View style={{width: "100%"}}>
                    <TouchableOpacity style={styles.PlayButton} onPress={this.playRecording.bind(this)} disabled={this.recording==null}>
                        <Text style={styles.PlayButtonText}>PLAY</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.PlayButton} onPress={this.playRecording.bind(this)} disabled={this.recording==null}>
                        <Text style={styles.PlayButtonText}>SAVE</Text>
                    </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }

    startRecording = async () => {
        const { status } = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
        if (status !== 'granted') return;

        this.setState({ isRecording: true });
        await Audio.setAudioModeAsync({
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,

        });
        const recording = new Audio.Recording();

        try {
            await recording.prepareToRecordAsync(Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY);
            this.startTimer();
            await recording.startAsync();
        } catch (error) {
            console.log(error);
            this.stopRecording();
        }

        this.recording = recording;

    }

    stopRecording = async () => {
        this.setState({ isRecording: false });
        try {
            this.setState({ timerOn: false });
            clearInterval(this.timer);
            console.log(this.recording.getURI())
            await this.recording.stopAndUnloadAsync();
        } catch (error) {
            // Do nothing -- we are already unloaded.
        }
    }

    async playRecording() {
        const soundObject = new Audio.Sound();
        try {
            await soundObject.loadAsync({ uri: this.recording.getURI() });
            await soundObject.setPositionAsync(0)
            await soundObject.playAsync();
            console.log("PLAYING")
            // Your sound is playing!
        } catch (error) {
            console.log("ERROR", error)
            // An error occurred!
        }

    }

    resetRecording = () => {
        this.deleteRecordingFile();
        this.recording = null;
    }

    handleOnPressIn = () => {
        this.startRecording();
    }

    handleOnPressOut = () => {
        this.stopRecording();
    }

    handlQueryChange = (query) => {
        this.setState({ query });
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#31b39a',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    welcomeText: {
        fontWeight: "bold",
        fontSize: 30,
        color: "#fff",
        marginBottom: 20,
        alignItems: "center",
        justifyContent: "center",
    },
    icon: {
        width: 50,
    },
    RecordButton: {
        backgroundColor: '#f4f4f4',
        width: 150,
        height: 150,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 100,
    },
    PlayButton: {
        backgroundColor: "#f4f4f4",
        borderRadius: 41,
        height: 52,
        alignSelf: 'center',
        justifyContent: 'center',
        width: '85%',
        marginTop: 20,
    },
    PlayButtonText: {
        color: "#000",
        alignSelf: 'center',
        padding: "10%",
        fontFamily: 'Poppins-Regular',
        fontSize: 24,

    },
});