import React, { Component } from 'react';
import { firebaseApp } from '../../firebase';
import { StyleSheet, Text, View, TextInput, ScrollView,TouchableOpacity, Alert, KeyboardAvoidingView } from 'react-native';
import i18n from 'i18n-js';
import { BACKEND_URL } from '../../config'

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: {
        message: ''
      }
    }
  }
  async checkCredentials(email, password) {
    var self = this;
    //let result = await firebaseApp.auth().createUserWithEmailAndPassword(email, password)
    await fetch(BACKEND_URL + '/users', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    }).then(response => {
      if (response.status === 201) {
        //Alert.alert("PASSSED RESPONDE", "PASSOU")
        this.props.navigation.navigate('SignInScreen');
      }else{
        JSON.stringify(response.json().then(function(data) {
          console.log(data);
          self.showErrorMessage(data.error.code);
       }))
      }
    });
  }
  showErrorMessage(code){
    
    var text = "";
    //console.log(code)
    switch (code){
      case "auth/invalid-email":
          text = i18n.t('AUTH_INVALID_EMAIL');
          break;
      case "auth/invalid-password":
          text = i18n.t('AUTH_INVALID_PASSWORD');
          break;
      case "auth/email-already-exists":
          text = i18n.t('AUTH_EMAIL_ALREADY_EXISTS');
          break;
    }
    Alert.alert(i18n.t('ERROR'), text);
  }

  async signUp() {
    const { email, password } = this.state;
    this.checkCredentials(email, password);
  }

  render() {
    this.props.navigation.setOptions({
      title: i18n.t('REGISTER'),
      //Sets Header text of Status Bar
      headerTitleStyle: {
        fontFamily: 'Poppins-Regular',
        fontSize: 30,
      },
    })

    return (
      <ScrollView style={styles.container}>
        <View style={styles.inputViewEmail} >
          <TextInput
            style={styles.inputText}
            placeholder="Email"
            placeholderTextColor="#aeaeae"
            autoCorrect={false}
            autoCompleteType="email"
            onChangeText={text => this.setState({ email: text })} />
        </View>
        <View style={styles.inputViewPassword} >
          <TextInput
            style={styles.inputText}
            placeholder="Password"
            placeholderTextColor="#aeaeae"
            autoCorrect={false}
            autoCompleteType="password"
            secureTextEntry={true}
            onChangeText={text => this.setState({ password: text })} />
        </View>
        <TouchableOpacity name="signUpBtn" style={styles.registerBtn} onPress={this.signUp.bind(this)}>
          <Text style={styles.registerText}>{i18n.t('REGISTER')} </Text>
        </TouchableOpacity>
      </ScrollView>

    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#31b39a',
  },
  inputViewEmail: {
    marginTop: "28%",
    width: "85%",
    backgroundColor: "#f4f4f4",
    borderRadius: 26,
    height: 52,
    alignSelf: 'center'
    //justifyContent: "center",
    //padding: 20
  },
  inputViewPassword: {
    width: "85%",
    backgroundColor: "#f4f4f4",
    borderRadius: 26,
    height: 52,
    marginTop: "10%",
    marginBottom: "20%",
    alignSelf: 'center'
    //justifyContent: "center",
    //padding: 20
  },
  inputText: {
    fontFamily: 'Poppins-Regular',
    marginLeft: 30,
    fontSize: 16,
    //marginTop: "5%",
    height: 50,
    color: "#000",
  },
  registerBtn: {
    backgroundColor: "#f4f4f4",
    borderRadius: 41,
    height: 52,
    alignSelf: 'center',
    justifyContent: "center",
    width: '85%'
  },
  registerText: {
    color: "#000",
    alignSelf: 'center',
    padding: "5%",
    fontFamily: 'Poppins-Regular',
    fontSize: 24
  },
});

export default SignUp;