import React, { Component, useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Picker,
  Alert,
  TouchableHighlight,
  ScrollView
} from 'react-native';
import i18n from 'i18n-js';
import * as Speech from 'expo-speech';
import { TextInput } from 'react-native-gesture-handler';
import { BACKEND_URL } from '../../config'
import { firebaseApp } from '../../firebase';
import { pushNewEntry, AllEntriesList, EntriesToSendList, saveEntriesLocally, putEntry, updateLocalEntry, getAllEntries, DeleteEntry } from '../model/DiaryEntryList'
import DiaryEntry from '../model/DiaryEntry';
import { checkConnectivity } from '../CheckConnectivity'
import * as IntentLauncher from 'expo-intent-launcher';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as MediaLibrary from 'expo-media-library';


export default class Secured extends Component {

  constructor(props) {
    super(props);
    if (this.props.route.params && this.props.route.params.entry) {
      this.state = {
        feedback_title: this.props.route.params.entry.title,
        feedback_text: this.props.route.params.entry.description,
        entry: this.props.route.params.entry,
        hasFile: this.props.route.params.entry.file!=undefined,
        file: this.props.route.params.entry.file
      }
    } else {
      this.state = {
        feedback_title: "",
        feedback_text: "",
        hasFile: false,
        file: undefined,
        isVideo:false,
        isImage: false,
      }
    }
  }
  render() {
    this.props.navigation.setOptions({
      title: i18n.t('SCREEN_DIARY_ENTRY_NEW_ENTRY'),
      //Sets Header text of Status Bar
      headerTitleStyle: {
        fontFamily: 'Poppins-Regular',
        fontSize: 30,
      },
    });
    const { navigate } = this.props.navigation
    console.log(this.props.route.name)
    console.log("AAAAAAAAAAA",this.state.entry)

    return (
      <ScrollView style={styles.container}>
        <View style={styles.top}>
          <View style={{ width: "85%", alignSelf: 'center' }}>
            <Text style={styles.TitleText}>{i18n.t('SCREEN_DIARY_ENTRY_TITLE')}</Text>
          </View>
          <TextInput style={styles.FeedbackTextArea} multiline={false} editable={true} value={this.state.feedback_title} onChangeText={text => this.setState({ feedback_title: text })} />
        </View>
        <View style={styles.middle}>
          <View style={styles.DescriptionToolBar}>
            <Text style={styles.TitleText}>{i18n.t('SCREEN_DIARY_ENTRY_DESCRIPTION')}</Text>
            <TouchableOpacity onPress={this.getPicture.bind(this)}>
              <Image style={styles.smallIconSquare} source={require('../../assets/ui/image_icon.png')} />
            </TouchableOpacity >
            <TouchableOpacity onPress={this.getVideo.bind(this)}>
              <Image style={styles.smallIconSquare} source={require('../../assets/ui/video_icon.png')} />
            </TouchableOpacity >
            <TouchableOpacity onPress={this.openAudioRecorder.bind(this)}>
              <Image style={styles.smallIconRectangle} source={require('../../assets/ui/microphone_icon.png')} />
            </TouchableOpacity >
            <TouchableOpacity onPress={this.clear.bind(this)}>
              <Text style={styles.ClearText}>{i18n.t('SCREEN_DIARY_ENTRY_CLEAR')}</Text>
            </TouchableOpacity >
          </View>
          <TextInput style={styles.DescriptionTextArea} multiline={true} editable={true} value={this.state.feedback_text} onChangeText={text => this.setState({ feedback_text: text })} />
        </View>
        {this.state.hasFile && <View style={{ width: "85%", alignSelf: 'center', flexDirection: 'row', marginTop: 5, justifyContent: 'space-between' }}>
          <Text style={styles.smallText}>File</Text>
          <View style={styles.urlTextBar}>
            <Text numberOfLines={1} style={styles.urlText}>{this.state.file}</Text>
          </View>
        </View>}
        <View style={styles.bottom}>
          <TouchableOpacity style={styles.button} onPress={this.saveEntry.bind(this)} disabled={this.state.feedback_text == "" || this.state.feedback_title == ""}>
            <Text style={styles.btnText}>{i18n.t('SCREEN_DIARY_ENTRY_SAVE')} </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={this.deleteEntry.bind(this)} disabled={this.state.entry == undefined}>
            <Text style={styles.btnText}>{i18n.t('SCREEN_DIARY_ENTRY_DELETE')} </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
  clear() {
    this.setState({ file: undefined, feedback_text: "", feedback_title: "" })
  }

  async checkCameraPermission() {
    await Permissions.askAsync(Permissions.CAMERA);
    const status = await Permissions.getAsync(Permissions.CAMERA);
    if (status.status === 'granted') {
      this.setState({ hasCameraPermission: true })
    }

    const rollStatus = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (rollStatus.status !== 'granted') {
      //alert('Sorry, we need camera roll permissions to make this work!');
    } else {
      this.setState({ hasRollPermission: true })
    }

  }

  getPicture() {
    var self = this
    ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false
    })
      .then(response => {
        console.log(response)
        if (!response.cancelled) {
          self.setState({ hasFile: true, file: response.uri, isImage: true, isVideo: false })
          ImagePicker.getCameraRollPermissionsAsync().then(function(){
            MediaLibrary.saveToLibraryAsync(response.uri)
          })
        }
      })
  }
  getVideo() {
    var self = this
    ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Videos,
      allowsEditing: false
    })
      .then(response => {
        console.log(response)
        if (!response.cancelled) {
          self.setState({ hasFile: true, file: response.uri, isVideo: true, isImage: false })
          ImagePicker.getCameraRollPermissionsAsync().then(function(){
            MediaLibrary.saveToLibraryAsync(response.uri)
          })
        }

      })
  }

  async componentDidMount() {
    await this.checkCameraPermission();
  }

  openAudioRecorder() {
    const { navigate } = this.props.navigation
    navigate("RecordAudioScreen")
  }

  saveEntry() {
    if(this.state.hasFile){
      Alert.alert("Diary entries cannot, currently, be saved with files")
    }
    
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    var formattedDate = day + '/' + month + '/' + year
    console.log(formattedDate)
    const uid = firebaseApp.auth().currentUser.uid
    const entry = new DiaryEntry(uid, this.state.feedback_title, this.state.feedback_text, [this.state.file], formattedDate, this.state._id)
    if (this.state.entry) {
      if (this.state.entry._id) {
        this.state.entry.timestamp = formattedDate
        this.state.entry.title = this.state.feedback_title
        this.state.entry.description = this.state.feedback_text
        this.state.entry.file= this.state.file
        putEntry(this.state.entry)
      } else {
        var newEntry = new DiaryEntry(uid, this.state.feedback_title, this.state.feedback_text, [this.state.file], formattedDate)
        updateLocalEntry(this.state.entry, newEntry, uid)
      }
    } else {
      pushNewEntry(entry, firebaseApp.auth().currentUser.uid)
    }
    const { navigate } = this.props.navigation
    getAllEntries(firebaseApp.auth().currentUser.uid).then(navigate("Diary"))

  }

  deleteEntry() {
    /*EntriesToSendList.length = 0
    AllEntriesList.length = 0
    saveEntriesLocally(firebaseApp.auth().currentUser.uid)
    Alert.alert("Warning", "Not yet implemented")*/
    const { navigate } = this.props.navigation
    DeleteEntry(this.state.entry)
    getAllEntries((firebaseApp.auth().currentUser.uid)).then(navigate("Diary"))

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#31b39a',
  },
  TitleText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 25,
    color: "#fff",
    maxWidth: "50%",
  },
  smallText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: "#fff",
    maxWidth: "50%",
  },
  urlText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    maxWidth: "90%",
    maxHeight: 30,
    color: "#fff",
    marginLeft: 10,
  },
  urlTextBar: {
    backgroundColor: '#6fcab8',
    borderRadius: 20,
    alignContent: 'center',
    width: "90%"
  },
  smallIcon: {
    width: 30,
    height: 30,
  },
  top: {
    //flex: 1,
    justifyContent: 'flex-start',
    marginTop: 20,
  },
  middle: {
    //flex: 1,
    justifyContent: 'center',
    marginTop: 10,
  },
  bottom: {
    //flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 10,
    alignItems: 'center',
    alignContent: 'space-around'
  },
  FeedbackTextArea: {
    backgroundColor: '#6fcab8',
    borderRadius: 20,
    height: 50,
    textAlignVertical: "top",
    width: "85%",
    alignSelf: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: "#fff",
    padding: 10,
  },
  DescriptionTextArea: {
    backgroundColor: '#6fcab8',
    borderRadius: 20,
    height: 200,
    textAlignVertical: "top",
    width: "85%",
    alignSelf: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: "#fff",
    padding: 10,
  },
  DescriptionToolBar: {
    flexDirection: 'row',
    minHeight: 50,
    maxHeight: 50,
    justifyContent: 'space-between',
    alignSelf: 'center',
    width: "85%"
  },
  button: {
    backgroundColor: "#f4f4f4",
    borderRadius: 41,
    height: 52,
    alignItems: "center",
    width: "85%",
    justifyContent: "center",
    marginTop: 20
  },
  btnText: {
    color: "#000",
    alignSelf: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 30
  },
  smallIconSquare: {
    marginTop: 5,
    width: 30,
    height: 30,
  },
  smallIconRectangle: {
    marginTop: 5,
    width: 18,
    height: 27,
  },
  ClearText: {
    color: "#fff",
    fontFamily: 'Poppins-Regular',
    fontSize: 25,
    height: 30,
  },
});